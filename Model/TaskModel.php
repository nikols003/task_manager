<?php
/**
 * Created by PhpStorm.
 * User: mdubina
 * Date: 5/3/17
 * Time: 2:06 PM
 */

namespace Model;


use Library\DbConnection;
use Library\Request;

class TaskModel
{

	public function add(Request $request)
	{
		$form = new TaskForm($request);
		if ($message = $form->valid())
			return $message;
		if ($request->files('img')['name'])
			$image = $this->download_image($request->files('img'));
		else
			$image = 'webroot/task_img/blank.jpg';
		$args = $form->return_array();
		$args['image'] = $image;
		$db = DbConnection::getInstance()->getPdo();
		$ths = $db->prepare('INSERT INTO task_db (user_name, email, text, image) VALUES (:user_name, :email, :text, :image)');
		$ths->execute($args);
		unset($_SESSION['task_form']);
		return "Task added!";
	}

	private function download_image(array $image)
	{
		$db = DbConnection::getInstance()->getPdo();
		$ths = $db->query('SELECT * FROM task_db');
		$img_name = $ths->fetchColumn().".";
		echo $img_name;
		$type = explode('.', $image['name'])[1];
		if ($type != 'png' && $type != 'jpg' && $type != 'gif')
			return 'webroot/task_img/blank.jpg';
		$img_name = 'task_img/'.$img_name.$type;
		move_uploaded_file($image['tmp_name'], $img_name);
		$this->resize_image($img_name, $type);
		return 'webroot/'.$img_name;
	}

	private function resize_image($img_name, $type)
	{
		if ($type == 'gif')
			return ;
		$width = 320;
		$height = 240;
		list($width_orig, $height_orig) = getimagesize($img_name);
		$ratio_orig = $width_orig / $height_orig;
		if ($width / $height > $ratio_orig)
			$width = $height * $ratio_orig;
		else
			$height = $width / $ratio_orig;

		if ($type == 'jpg')
			$type = 'jpeg';
		$func = "imagecreatefrom".$type;
		$image_p = imagecreatetruecolor($width, $height);
		$image = $func($img_name);
		imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
		$func = "image".$type;
		$func($image_p, $img_name);
	}

	public function findAll($type_sort)
	{
		$db = DbConnection::getInstance()->getPdo();
		var_dump($type_sort);
		if ($type_sort == 'name')
			$ths = $db->query('SELECT * FROM task_db ORDER by user_name');
		elseif ($type_sort == 'status')
			$ths = $db->query('SELECT * FROM task_db ORDER by status DESC');
		else
			$ths = $db->query('SELECT * FROM task_db ORDER by email');
		$tasks = $ths->fetchAll(\PDO::FETCH_ASSOC);
		return $tasks;
	}

	public function changeStatus($id, $status)
	{
		$db = DbConnection::getInstance()->getPdo();
		$ths = $db->prepare('UPDATE task_db SET status = :status WHERE id = :id');
		$ths->execute(array('id' => $id, 'status' => $status));
	}

	public function editText($id, $text)
	{
		$db = DbConnection::getInstance()->getPdo();
		$ths = $db->prepare('UPDATE task_db SET text = :text WHERE id = :id');
		$ths->execute(array('id' => $id, 'text' => $text));
	}


}