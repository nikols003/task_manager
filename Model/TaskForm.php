<?php
/**
 * Created by PhpStorm.
 * User: mdubina
 * Date: 5/3/17
 * Time: 6:16 PM
 */

namespace Model;


use Library\Request;

class TaskForm
{
	public $user_name;
	public $email;
	public $text;

	public function __construct(Request $request)
	{
		$this->user_name = $request->post('user_name');
		$this->email = $request->post('email');
		$this->text = $request->post('text');
		$_SESSION['task_form'] = array($this->user_name, $this->email, $this->text);
	}

	public function valid()
	{
		if ($this->user_name == '')
			return "Name cannot be empty!";
		if ($this->email == '')
			return "email cannot be empty!";
		if ($this->text == '')
			return "text cannot be empty!";
	}

	public function return_array()
	{
		return array('user_name' => $this->user_name, 'email' => $this->email, 'text' => $this->text);
	}

}