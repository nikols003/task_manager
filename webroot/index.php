<?php

	ini_set('display_errors', 1);
	error_reporting(E_ALL);


	spl_autoload_register(function ($name) {
		$name = str_replace('\\', '/', $name);
		require '../'.$name.'.php';});
	session_start();

	try {
		$request = new \Library\Request();
		$controller = $request->getURI();
		$str = explode('/', $controller);
		$controller = $str[1] == '' ? 'Index' : ucfirst($str[1]);
		$action = isset($str[2]) ? ucfirst($str[2]) : 'Index';
		$controller = "/Controller/" . $controller . "Controller";
		if (!file_exists('../' . $controller . '.php'))
			throw new Exception("{$controller} not found", 404);
		$controller = str_replace('/', '\\', $controller);
		$controller = new $controller;
		$action = $action . "Action";
		if (!method_exists($controller, $action))
			throw new Exception("{$action} not found", 404);
		else
			$content = $controller->$action($request);
	}
	catch(\Exception $e) {
		$content = \Library\Controller::renderError($e->getCode(), $e->getMessage());
	}

	echo $content;

?>