<?php

namespace Library;


/**
 * Singleton Pattern
 */
class DbConnection
{
    private static $instance = null;

    /**
     * @var \PDO
     */
    private $pdo;

    private function __construct()
    {
    	require '../Config/database1.php';
    	$this->pdo = new \PDO("mysql:host=$DB_DSN;dbname=$DB_NAME", $DB_USER, $DB_PASSWORD);
    }


    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new DbConnection();
        }

        return self::$instance;
    }


    /**
     * @return \PDO
     */
    public function getPdo()
    {
        return $this->pdo;
    }
}

