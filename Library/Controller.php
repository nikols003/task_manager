<?php

namespace Library;


abstract class Controller
{
	protected $content = "";

    private static $layout = '../View/layout.phtml';

//    public static function setAdminLayout()
//    {
//        self::$layout = 'admin_layout.phtml';
//    }

    protected function render($viewName, array $args = array())
    {
        extract($args);

		ob_start();
		if ($viewName != '')
        	require $viewName;
        $page = isset($page) ? $page : "";
        $content = ob_get_clean();
		ob_start();
        require self::$layout;
		return ob_get_clean();
    }

	public static function renderError($code, $message)
	{
		ob_start();
		require '../View/error.phtml';
		$content = ob_get_clean();

		ob_start();
		require self::$layout;
		return ob_get_clean();
	}
}