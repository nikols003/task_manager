<?php
/**
 * Created by PhpStorm.
 * User: mdubina
 * Date: 5/4/17
 * Time: 3:53 PM
 */

namespace Library;


trait Pagination
{
	public function pagination(array $tasks, $page)
	{
		$array = array();
		$pages = array();
		$elem_on_page = 3;
		$max_page = ceil(count($tasks) / $elem_on_page);
		if (!$page || $page > $max_page)
			$page = 1;
		for ($i = 0; $i < $elem_on_page; $i++)
			if (isset($tasks[(($page - 1) * 3) + $i]))
				$array[] = $tasks[(($page - 1) * 3) + $i];
		if (empty($array))
			return compact('pages', 'array');

		$pages = array('page' => $page, 'max_page' => $max_page, 'next' => $page + 1, 'prev' =>
			$page - 1);
		return compact('pages', 'array');
	}
}