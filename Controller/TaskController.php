<?php
/**
 * Created by PhpStorm.
 * User: mdubina
 * Date: 5/3/17
 * Time: 2:05 PM
 */

namespace Controller;


use Library\Controller;
use Library\Request;
use Model\TaskModel;

class TaskController extends Controller
{

	public function AddAction(Request $request)
	{
		$message = '';
		if ($request->post('submit') == 'Add task') {
			$task = new TaskModel($request);
			$message = $task->add($request);
		}
		return $this->render('../View/task_form.phtml', array('message' => $message));
	}

	public function PreviousAction(Request $request)
	{
		if ($_FILES['img']['tmp_name'] != '')
		{
			move_uploaded_file($_FILES['img']['tmp_name'], 'task_img/tmp.png');
			echo '/webroot/task_img/tmp.png';
		}
		else
			echo '/webroot/task_img/blank.jpg';
	}
}