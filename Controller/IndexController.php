<?php
/**
 * Created by PhpStorm.
 * User: mdubina
 * Date: 5/3/17
 * Time: 5:15 PM
 */

namespace Controller;


use Library\Controller;
use Library\Pagination;
use Library\Request;
use Model\TaskModel;

class IndexController extends Controller
{
	use Pagination;
	public function IndexAction(Request $request)
	{
		$model = new TaskModel($request);
		$tasks = $model->findAll($request->get('sort'));
		$args =  $this->pagination($tasks, $request->get('page'));
		$args['sort'] = $request->get('sort');
		return $this->render('../View/index.phtml', $args);
	}

}