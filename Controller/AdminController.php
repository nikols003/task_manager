<?php
/**
 * Created by PhpStorm.
 * User: mdubina
 * Date: 5/4/17
 * Time: 10:49 AM
 */

namespace Controller;


use Library\Controller;
use Library\Pagination;
use Library\Request;
use Model\TaskModel;

class AdminController extends Controller
{
	use Pagination;

	public function IndexAction(Request $request)
	{
		$args = array('message' => "");
		if (!isset($_SESSION['login']))
			return $this->render('../View/admin_form.phtml', $args);
		else {
			$model = new TaskModel($request);
			$tasks = $model->findAll($request->get('sort'));
			$args = $this->pagination($tasks, $request->get('page'));
			$args['sort'] = $request->get('sort');
			return $this->render('../View/admin_index.phtml', $args);
		}
	}

	public function LoginAction(Request $request)
	{
		if ($request->post('login') != 'admin')
			return $this->render('../View/admin_form.phtml', array('message' => "Incorrect login"));
		else if ($request->post('password') != '123')
			return $this->render('../View/admin_form.phtml', array('message' => "Incorrect password"));
		else
		{
			$_SESSION['login'] = 'Admin';
			header('Location: /admin/index');
		}
	}

	public function ChangeAction(Request $request)
	{
		$model = new TaskModel();
		$model->changeStatus($request->post('id'), $request->post('change'));
		header('Location: /admin/index');
	}

	public function EditAction(Request $request)
	{
		$model = new TaskModel();
		$model->editText($request->post('id'), $request->post('text'));
		header('Location: /admin/index');
	}
}